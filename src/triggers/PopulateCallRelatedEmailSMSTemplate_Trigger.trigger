trigger PopulateCallRelatedEmailSMSTemplate_Trigger on Call__c (before insert, before update) {
	
    /*
    if (Trigger.isInsert) {
        Set<Call__c> insCallRecords = new Set<Call__c>();
        Map<Call__c, String> callToCallTemplateIdMap = new Map<Call__c, String>();
        List<Call_Template__c> relatedCallTemplateList = new List<Call_Template__c>();
        Map<String, List<String>> callTemplateIdToIDListMap = new Map<String, List<String>>();
        Set<String> cCallTemplateIdSet = new Set<String>();
        Set<String> ctEmailTemplateIdSet = new Set<String>();
        Set<String> ctSMSTemplateIdSet = new Set<String>();
        for (Call__c trgCall : Trigger.new) {
            if(trgCall.Call_Template__c != null){
                insCallRecords.add(trgCall);
                cCallTemplateIdSet.add(trgCall.Call_Template__c);
                callToCallTemplateIdMap.put(trgCall, trgCall.Call_Template__c);
            }
        }
            
        if(!cCallTemplateIdSet.isEmpty()){
            relatedCallTemplateList = [SELECT Id, Name, Attempt__c, Email_Template__c, SMS_Template__c FROM Call_Template__c WHERE Id IN: cCallTemplateIdSet];
        }
        
        for(Call_Template__c ct : relatedCallTemplateList){
            List<String> tempIds = new String[2];
            tempIds.add(0, ct.Email_Template__c);
            tempIds.add(1, ct.SMS_Template__c);
            callTemplateIdToIDListMap.put(ct.Id, tempIds);    
        }
        
        for(Call__c trgCall2 : insCallRecords) {
            List<String> tempIdList = callTemplateIdToIDListMap.get(callToCallTemplateIdMap.get(trgCall2));
            trgCall2.Email_Template__c = tempIdList.get(0);
            trgCall2.SMS_Template__c = tempIdList.get(1);
        }

	    
	} else if (Trigger.isUpdate) {
        Map<Call__c, String> callToCallTemplateIdMap = new Map<Call__c, String>();
        List<Call_Template__c> relatedCallTemplateList = new List<Call_Template__c>();
        Map<String, List<String>> callTemplateIdToIDListMap = new Map<String, List<String>>();
        Set<String> cCallTemplateIdSet = new Set<String>();
        Set<String> ctEmailTemplateIdSet = new Set<String>();
        Set<String> ctSMSTemplateIdSet = new Set<String>();
        Set<Call__c> updCallRecords = new Set<Call__c>();
        Set<Call__c> updCallRecords2 = new Set<Call__c>();
        for (Call__c newTrgCall : Trigger.new) {
            Call__c oldCall = Trigger.oldMap.get(newTrgCall.Id);
            if(newTrgCall.Call_Template__c != oldCall.Call_Template__c){
                updCallRecords.add(newTrgCall);
            }
        }
        
        if(!updCallRecords.isEmpty()){
        	for (Call__c trgCall : updCallRecords) {
            	if(trgCall.Call_Template__c != null){
                    updCallRecords2.add(trgCall);
               		cCallTemplateIdSet.add(trgCall.Call_Template__c);
                	callToCallTemplateIdMap.put(trgCall, trgCall.Call_Template__c);
            	}
        	}
    	}    
		
        if(!cCallTemplateIdSet.isEmpty()){
            relatedCallTemplateList = [SELECT Id, Name, Attempt__c, Email_Template__c, SMS_Template__c FROM Call_Template__c WHERE Id IN: cCallTemplateIdSet];
        }
        
        for(Call_Template__c ct : relatedCallTemplateList){
            List<String> tempIds = new String[2];
            tempIds.add(0, ct.Email_Template__c);
            tempIds.add(1, ct.SMS_Template__c);
            callTemplateIdToIDListMap.put(ct.Id, tempIds);    
        }
        
        for(Call__c trgCall2 : updCallRecords2) {
            List<String> tempIdList = callTemplateIdToIDListMap.get(callToCallTemplateIdMap.get(trgCall2));
            trgCall2.Email_Template__c = tempIdList.get(0);
            trgCall2.SMS_Template__c = tempIdList.get(1);
        }
	
	    
		}
	

    */

}