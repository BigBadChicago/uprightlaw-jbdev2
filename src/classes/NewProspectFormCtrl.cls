public with sharing class NewProspectFormCtrl {
    
    @AuraEnabled
	public static Call__c saveProspect(Prospect__c prospect) {
    	// Perform isUpdateable() check here 
    	prospect.Name = prospect.First_Name__c + ' ' + prospect.Last_Name__c;
    	insert prospect;
        Call_Pattern__c callPat = [SELECT Id, Name, Active__c FROM Call_Pattern__c WHERE Active__c = true LIMIT 1];
        Call_Template__c callTemp = [SELECT Id, Name, Attempt__c, Call_Pattern__c, Team__c, Email_Template__c, SMS_Template__c FROM Call_Template__c WHERE Attempt__c = 1 AND Call_Pattern__c =: callPat.Id LIMIT 1];
        
    	Call__c newCallViaNewProspect = new Call__c();
		List<RecordType> RT_IDs = [SELECT Id FROM RecordType WHERE Name = 'Prospect Call' AND sObjectType = 'Call__c'];
        Id RT_ID = RT_IDs[0].Id;
        
        newCallViaNewProspect.Prospect__c = prospect.Id;
    	newCallViaNewProspect.RecordTypeId = RT_ID;
        newCallViaNewProspect.Attempt__c = 1;
        newCallViaNewProspect.Status__c = 'Not Started';
        newCallViaNewProspect.Inbound_Call__c = True;
        newCallViaNewProspect.Locked__c = True;
        newCallViaNewProspect.Locked_Time__c = Datetime.now(); 
        newCallViaNewProspect.Locked_By__c = userInfo.getUserId();
        newCallViaNewProspect.Call_Pattern__c = callPat.Id;
        newCallViaNewProspect.Call_Template__c = callTemp.Id;
        newCallViaNewProspect.Team__c = callTemp.Team__c;
        newCallViaNewProspect.Earliest_Call_Time__c = System.now();
        //**** NOTE:  The Email and SMS Templates are only Populated after an Email or SMS Message 
        //newCallViaNewProspect.Email_Template__c = callTemp.Email_Template__c;
        //newCallViaNewProspect.SMS_Template__c = callTemp.SMS_Template__c;
        insert newCallViaNewProspect;
    	return newCallViaNewProspect;
	}
    

}