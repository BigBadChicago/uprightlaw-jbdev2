public with sharing class ContactInfoController {
    
    @AuraEnabled
    public static Prospect__c fetchProspect(String prospectId) {
        Prospect__c p = [SELECT First_Name__c, Last_Name__c, Home_Phone__c, Work_Phone__c, Mobile_SMS_Phone__c,
        				Personal_Email__c, State__c, Postal_Code__c FROM Prospect__c
        				WHERE Id = :prospectId
        				 ];
        return p; 
    }

    @AuraEnabled
	public static Prospect__c saveProspect(Prospect__c prospect) {
    upsert prospect;
    return prospect;
	}
}