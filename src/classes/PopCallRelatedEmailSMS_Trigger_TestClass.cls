@isTest
private class PopCallRelatedEmailSMS_Trigger_TestClass {

//     @testSetup static void setupTestData(){
//     	RecordType prsRecType = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'Prospect__c' AND DeveloperName = 'Bankruptcy' LIMIT 1];
    	
//         Profile p = [SELECT Id FROM Profile WHERE Name='Case Manager']; 
//       	// CREATING TEST USERS
//         List<User> usrList = new List<User>();
//         for( Integer i = 0; i < 6; i++ ){
//             User u = new User(
//                 Alias = 'testUsr' + i, 
//                 Email='testUsr' + i + '@testorg.com', 
//                 EmailEncodingKey='UTF-8', 
//                 LastName='Testing' + i, 
//                 LanguageLocaleKey='en_US', 
//                 LocaleSidKey='en_US', 
//                 ProfileId = p.Id, 
//                 TimeZoneSidKey='America/Chicago',   
//                 UserName='testUsr' + i + '@UprightLawtestorg.com');
//             usrList.add(u);
//         }
//         insert usrList; 
// 		// CREATING TEST STATES
//     	State__c testState = new State__c(Name = 'Illinois', State_Entity__c = 'UpRight Law LLC', Case_Manager__c = usrList[3].Id, Chapter_13_Internal_Attorney__c = usrList[4].Id, Chapter_7_Internal_Attorney__c = usrList[5].Id ,Abbreviation__c = 'XX', Outbound_CallerID__c = '3125551212');
//         insert testState;
//         // CREATING TEST TIME ZONES
//         Time_Zone__c tz = new Time_Zone__c(Name='America/Chicago', Standard_Offset__c = -6, DST__c = true);
// 	    insert tz;
//         Time_Zone__c test_tz = new Time_Zone__c(Name='America/Los_Angeles', Standard_Offset__c = -8, DST__c = true);
// 		insert test_tz;
//         // CREATING TEST ZIP CODES
//         Zip_Code__c test_z1 = new Zip_Code__c(Name='60056', Time_Zone__c=tz.Id);
//         insert test_z1;
//         Zip_Code__c test_z2 = new Zip_Code__c(Name='90210', Time_Zone__c=test_tz.Id);
//         insert test_z2;
//         Call_Pattern__c testCallPattern = new Call_Pattern__c( Name = 'testCallPattern', Active__c = true);
//         insert testCallPattern;
//         Call_Template__c testCallTemplate = new Call_Template__c( Attempt__c = 1, Call_Pattern__c = testCallPattern.Id, Team__c = 'A');
//         insert testCallTemplate;
        
//         // CREATING TEST SMS TEMPLATE
//         SMS_Template__c sms = new SMS_Template__c();
//         sms.Name = 'Supercool';
//         sms.SMS_Message__c = 'Lorem ipsum dolor sit amet';
		
//         insert sms;
//         // CREATING TEST EMAIL TEMPLATE
//         Email_Template__c eml = new Email_Template__c();
//         eml.Name = 'McLovin';
//         eml.HTML_Email__c = 'Lorem ipsum dolor sit amet';
		
//         insert eml;
//         // CREATING TEST CALL TEMPLATE
//         Call_Template__c ct = new Call_Template__c(); 
//         ct.Call_Pattern__c = testCallPattern.Id;
//         ct.Attempt__c = 1;
//         ct.Min_Hours_Since_Previous_Call__c = 0;
//         ct.Team__c = 'A';
//         ct.SMS_Template__c = sms.Id;
//         ct.Email_Template__c = eml.Id;
//         insert ct;
//         // CREATING TEST PROSPECTS
        
//         Prospect__c prospect = new Prospect__c(
//                 Name = 'Test Prospect' ,
//                 First_Name__c = 'Test',
//                 Last_Name__c = 'Prospect',
//                 Home_Phone__c = '123-123-1234',
//                 Work_Phone__c = '987-987-9876',
//                 Mobile_SMS_Phone__c = '454-454-4545',
//                 Personal_Email__c = 'TestProspect1@testorg.com',
//                 Street__c = '1910 N Elm Street',
//                 City__c = 'Springfield',
//                 Postal_Code__c = '60056',
//                 State__c = 'XX', 
//                 RecordTypeId = prsRecType.Id);
       
//         insert prospect;
      
        
       
//     }


    
//     static testMethod void testInsertCall() {
//         // CREATING TEST CALL PATTERN
//         Call_Pattern__c cp = new Call_Pattern__c();
//         cp.Active__c = true;
//         cp.Name = 'Test_Call_Pattern';
//         cp.Unlimited_Calls__c = true;
//         cp.Calls__c = null;
//         cp.Consults__c = null;
            
//         insert cp;
//         // CREATING TEST SMS TEMPLATE
//         SMS_Template__c sms = new SMS_Template__c();
//         sms.Name = 'Supercool';
//         sms.SMS_Message__c = 'Lorem ipsum dolor sit amet';
		
//         insert sms;
//         // CREATING TEST EMAIL TEMPLATE
//         Email_Template__c eml = new Email_Template__c();
//         eml.Name = 'McLovin';
//         eml.HTML_Email__c = 'Lorem ipsum dolor sit amet';
		
//         insert eml;
//         // CREATING TEST CALL TEMPLATE
//         Call_Template__c ct = new Call_Template__c(); 
//         ct.Call_Pattern__c = cp.Id;
//         ct.Attempt__c = 1;
//         ct.Min_Hours_Since_Previous_Call__c = 0;
//         ct.Team__c = 'A';
//         ct.SMS_Template__c = sms.Id;
//         ct.Email_Template__c = eml.Id;
//         insert ct;
//         Prospect__c prspt = [SELECT Id, Name FROM Prospect__c LIMIT 1];

//         Call__c newCall = new Call__c();
//         newCall.Status__c = 'Not Started';
//         newCall.Attempt__c  = 1; 
//       	newCall.Call_Pattern__c = cp.Id;
//         newCall.Call_Template__c = ct.Id;
//         newCall.Prospect__c = prspt.Id ; 
//         newCall.Team__c = 'A';
//         insert newCall;
//         List<Call__c> testCallList = [SELECT Id, Name, Attempt__c, Call_Pattern__c, Locked__c, Call_Template__c, Prospect__c, Email_Template__c, SMS_Template__c, Status__c, Team__c FROM Call__c WHERE Prospect__c =: prspt.Id LIMIT 1];
//     	System.assertEquals(1, testCallList.size(), 'Prospect Call List does NOT return the right number of items.  ');
//         System.assertEquals(sms.Id, testCallList[0].SMS_Template__c);
//     }
    
//     static testMethod void testUpdateCall() {
//         // CREATING TEST CALL PATTERN
//         Call_Pattern__c cp = new Call_Pattern__c();
//         cp.Active__c = true;
//         cp.Name = 'Test_Call_Pattern';
//         cp.Unlimited_Calls__c = true;
//         cp.Calls__c = null;
//         cp.Consults__c = null;
            
//         insert cp;
//         // CREATING TEST SMS TEMPLATE
//         SMS_Template__c sms = new SMS_Template__c();
//         sms.Name = 'Supercool';
//         sms.SMS_Message__c = 'Lorem ipsum dolor sit amet';
		
//         insert sms;
//         // CREATING TEST EMAIL TEMPLATE
//         Email_Template__c eml = new Email_Template__c();
//         eml.Name = 'McLovin';
//         eml.HTML_Email__c = 'Lorem ipsum dolor sit amet';
		
//         insert eml;
//         // CREATING TEST CALL TEMPLATE
//         Call_Template__c ct = new Call_Template__c(); 
//         ct.Call_Pattern__c = cp.Id;
//         ct.Attempt__c = 1;
//         ct.Min_Hours_Since_Previous_Call__c = 0;
//         ct.Team__c = 'A';
//         ct.SMS_Template__c = sms.Id;
//         ct.Email_Template__c = eml.Id;
//         insert ct;
//         Prospect__c prspt = [SELECT Id, Name FROM Prospect__c LIMIT 1];
//         // CREATING TEST SMS TEMPLATE
//         SMS_Template__c sms2 = new SMS_Template__c();
//         sms2.Name = 'Awesome Dude';
//         sms2.SMS_Message__c = 'Lorem ipsum dolor sit amet';
// 		insert sms2;
//         // CREATING TEST EMAIL TEMPLATE
//         Email_Template__c eml2 = new Email_Template__c();
//         eml2.Name = 'Vincent Adultman';
//         eml2.HTML_Email__c = 'Lorem ipsum dolor sit amet';
// 		insert eml2;
//         // CREATING TEST CALL TEMPLATE
//         Call_Template__c ct2 = new Call_Template__c(); 
//         ct2.Call_Pattern__c = cp.Id;
//         ct2.Attempt__c = 2;
//         ct2.Min_Hours_Since_Previous_Call__c = 0;
//         ct2.Team__c = 'A';
//         ct2.SMS_Template__c = sms2.Id;
//         ct2.Email_Template__c = eml2.Id;
//         insert ct2;

        
        
        
        
//         Call__c newCall = new Call__c();
//         newCall.Status__c = 'Not Started';
//         newCall.Attempt__c  = 1; 
//         newCall.Call_Pattern__c = cp.Id;
//         newCall.Call_Template__c = ct.Id;
//         newCall.Prospect__c = prspt.Id ; 
//         newCall.Team__c = 'A';
//         insert newCall;
//         List<Call__c> testCallList = [SELECT Id, Name, Attempt__c, Call_Pattern__c, Locked__c, Call_Template__c, Prospect__c, Email_Template__c, SMS_Template__c, Status__c, Team__c FROM Call__c WHERE Prospect__c =: prspt.Id LIMIT 1];
//     	System.assertEquals(1, testCallList.size(), 'Prospect Call List does NOT return the right number of items.  ');
//         System.assertEquals(sms.Id, testCallList[0].SMS_Template__c);
//         Call__c updCall = testCallList[0];
//         updCall.Call_Template__c = ct2.Id;
//         update updCall;
//         List<Call__c> testCallList2 = [SELECT Id, Name, Attempt__c, Call_Pattern__c, Locked__c, Call_Template__c, Prospect__c, Email_Template__c, SMS_Template__c, Status__c, Team__c FROM Call__c WHERE Prospect__c =: prspt.Id LIMIT 1];
//     	System.assertEquals(1, testCallList.size(), 'Prospect Call List does NOT return the right number of items.  ');
        
        
        
//     }
    
}