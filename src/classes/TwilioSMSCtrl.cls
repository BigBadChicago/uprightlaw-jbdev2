public with sharing class TwilioSMSCtrl {

    
    // This is the initial method that gets called when the TwilioResponse page loads
    public PageReference init()
    {
        String fromNumber = ApexPages.currentPage().getParameters().get('From');
        String toNumber = ApexPages.currentPage().getParameters().get('To');
        String body = ApexPages.currentPage().getParameters().get('Body');
        PageReference response = new PageReference('http://uprightlaw.com');
        // Helpful with debugging
        System.debug('FROM: ' + fromNumber);
        System.debug('TO: ' + toNumber);
        System.debug('BODY: ' + body);
 
        // If we capture all of the parameters successfully, call the saveIncomingText() method
        if(fromNumber != null && toNumber != null && body != null)
        {
            response = saveIncomingText();
            return response;
        }
 
        return null;
    }
    
    public PageReference saveIncomingText() {
        String fromNumber = ApexPages.currentPage().getParameters().get('From');
        String toNumber = ApexPages.currentPage().getParameters().get('To');
        String body = ApexPages.currentPage().getParameters().get('Body');
        // Format the incoming number for Prospect Search
        String fromSMSNumber = formatPhone(fromNumber);
        
        // Find the Prospect whose Cell Phone matches the fromNumber os the SMS message
        Prospect__c smsProspect = [SELECT Id, Name, Mobile_SMS_Phone__c, SMS_Disclaimer_Sent__c, DNC_SMS__c FROM Prospect__c WHERE ( Mobile_SMS_Phone__c =: fromSMSNumber OR Home_Phone__c =: fromSMSNumber ) AND SMS_Disclaimer_Sent__c = true AND DNC_SMS__c =false LIMIT 1];
        //  **************************************
        //  ****  THIS MAY NOT WORK DUE TO TWILIO'S OWN HANDLING OF SMS OPT-OUT REQUESTS ****
        //  ****  TWILIO RESPONDS AND HANDLES OPT-OUT REQUESTS BEFORE THE MESSAGE IS SENT TO US ****
        Boolean stopTest = Pattern.matches('([S,s][T,t][O,o][P,p])', body);
        if(stopTest == true){
            smsProspect.DNC_SMS__c = true;
        }
        //  ****************************************
        
        
        // Create a SMS_Message to store the record of the incoming text, then attach it to the appropriate Contact
        Incoming_SMS_Message__c newIncomingSMS = new Incoming_SMS_Message__c();
        newIncomingSMS.From_Number__c = fromSMSNumber;
        newIncomingSMS.Inbound_Number__c = formatPhone(toNumber);
        newIncomingSMS.Incoming_SMS_Message__c = body;
        newIncomingSMS.Prospect__c = smsProspect.Id;
 
        // Insert the Incoming_SMS_Message__c record 
        try{
             
            sendResponseTextMessage(formatPhone(toNumber), fromSMSNumber);
            insert newIncomingSMS;  
            if(stopTest == true){
                update smsProspect;
            }
            return null;
        } catch(DmlException e){
            System.debug('INSERT TASK FAILED: ' + e);
            return null;
        }
    }
    
    public String sendProspectTextMessage(String incomingCallID, String incomingProspectID){
    //****  public void sendProspectTextMessage(Set<Call__c> trgCallSet, Set<ID> trgProspectIDSet){
        String fromSMSNumber = null;
        String toSMSNumber = null;
        TwilioRestClient client;
        if(!Test.isRunningTest()){
            client = TwilioAPI.getDefaultClient();
        } else {
            String ACCOUNT_SID = 'ACe35fa95cdf61c8bb333d19b6d74c7c30';
            String AUTH_TOKEN = '30026fb2937f1a83808d0fea3f7d7cdd';
            client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
        }

        SMS_Template__c disclaimerMSG = [SELECT Id, Name, SMS_Message__c FROM SMS_Template__c WHERE Name = 'Disclaimer' LIMIT 1];
        List<TwilioMessage> SMSMessageList = new List<TwilioMessage>();
        Map<Id, Prospect__c> ProspectMap = new Map<Id, Prospect__c>([SELECT Id, Name, DNC__c, First_Name__c, Mobile_SMS_Phone__c, Home_Phone__c, Zip_Code_Lookup__c, DNC_SMS__c, SMS_Disclaimer_Sent__c, Zip_Code_Lookup__r.SMS_Phone_Number__c FROM Prospect__c WHERE Id =: incomingProspectID]);
        Call__c c = [SELECT Id, Name, DNC__c, Prospect__c, Prospect__r.Id, Call_Template__c, Call_Template__r.SMS_Template__c, Call_Template__r.SMS_Template__r.SMS_Message__c FROM Call__c WHERE Id =: incomingCallID LIMIT 1];
        //*** for(Call__c c : [SELECT Id, Name, DNC__c, Prospect__c, Prospect__r.Id, Call_Template__c, Call_Template__r.SMS_Template__c, Call_Template__r.SMS_Template__r.SMS_Message__c FROM Call__c WHERE Id =: incomingCallID]){
        //*** for(Call__c c : trgCallSet){
            Prospect__c callProspect = ProspectMap.get(c.Prospect__r.Id);
            //*** Using a conditional statement to find the number that will be used as the TO: parameter
            //*** Prospect's Mobile_SMS_Phone__C number will be used unless it's null, in which the 
            //*** Prospect's Home_Phone__c will be used 
            
            if(callProspect.Mobile_SMS_Phone__c != null){
                toSMSNumber = callProspect.Mobile_SMS_Phone__c;
            } else{
                toSMSNumber = callProspect.Home_Phone__c;
            }
            if(!Test.isRunningTest()){
                fromSMSNumber = callProspect.Zip_Code_Lookup__r.SMS_Phone_Number__c;
            } else {
                fromSMSNumber = '"+15005550006';
            }
            String salutation = 'Hi ' + callProspect.First_Name__c + ', ';
            String smsBody = c.Call_Template__r.SMS_Template__r.SMS_Message__c;
            String disclaimer = disclaimerMSG.SMS_Message__c;
            List<TwilioNameValuePair> properties = new List<TwilioNameValuePair>();
            
            //****  Helpful with debugging
            System.debug('*** JB  *** FROM: ' + fromSMSNumber);
            System.debug('*** JB *** TO: ' + toSMSNumber);
            System.debug('*** JB *** BODY1: ' + salutation + smsBody);
            System.debug('*** JB *** BODY1: ' + disclaimer);
            //****  Send disclaimer message
            if(smsBody != null){
                if(callProspect.SMS_Disclaimer_Sent__c == false){
                    try{
                        List<TwilioNameValuePair> propDisclaimer = new List<TwilioNameValuePair>();
                        propDisclaimer.add(new TwilioNameValuePair('To', toSMSNumber));
                        propDisclaimer.add(new TwilioNameValuePair('From', fromSMSNumber));
                        propDisclaimer.add(new TwilioNameValuePair('Body', disclaimer));
                        //**** From the messages sent, get the TO phone number in the Twilio format (i.e. +13125551212)
                        TwilioMessage disclmrMessage = client.getAccount().getMessages().create(propDisclaimer);
                        SMSMessageList.add(disclmrMessage);  
                    } catch(exception e){
                        System.debug('***JB*** SMS Message Error ==========>'+e);
                    }        
                    
                }
                //****  Send first Message
                if(smsBody != null){
                    try{
                        properties.add(new TwilioNameValuePair('To', toSMSNumber));
                        properties.add(new TwilioNameValuePair('From', fromSMSNumber));
                        properties.add(new TwilioNameValuePair('Body', salutation + smsBody));
                        TwilioMessage message = client.getAccount().getMessages().create(properties);
                        SMSMessageList.add(message);
                    } catch(exception e){
                        System.debug('***JB*** SMS Message Error ==========>'+e);
                    }    
                }
                //***** Create Task Activity to document that a SMS Message was sent
                Task tsk = new Task();
                tsk.Subject = 'SMS to Prospect'; 
                tsk.Type = 'SMS Message Sent';
                tsk.Status = 'Completed';
                tsk.OwnerId = UserInfo.getUserId();
                tsk.WhatId = callProspect.Id;
                tsk.Call__c = c.Id;
                tsk.ActivityDate = date.today(); 
                tsk.Description = smsBody;
                insert tsk;
                //***** Update the Call record with the SMS Template that was used to send the SMS Message
                c.SMS_Template__c = c.Call_Template__r.SMS_Template__c;
                update c;
            }
            //****  Update the Prospect's Mobile Number with the TO phone number in the clean (312) 555-1212 format 
            //****  so it's standardized for incoming SMS messages
            // TODO !  If message was successful, then update sms phone number
            //*** callProspect.Mobile_SMS_Phone__c = formatPhone(message.getTo());
            //*** Prospect :  Sent Disclaimer Text = true
            //*** Prospect :  Stop SMS if STOP response = true
            // END TODO
        // }
        //    update ProspectMap.values();
            System.debug('***JB*** SMSMessage List: ' + SMSMessageList);
            return String.valueOf(SMSMessageList.size());
    }
    
    
    public void updateProspectPostSMS(String msgListNumber, String incomingProspectID){
        system.debug('***JB2** ProspectID: '+ incomingProspectID);
        SYSTEM.debug('***JB2** msgListNumber: ' + msgListNumber);
        
        //Prospect__c curProspect = [SELECT Id, Name, DNC__c, DNC_SMS__c, First_Name__c, SMS_Disclaimer_Sent__c, Mobile_SMS_Phone__c, Home_Phone__c, Zip_Code_Lookup__c, Zip_Code_Lookup__r.SMS_Phone_Number__c FROM Prospect__c WHERE Id =: incomingProspectID LIMIT 1];
        //Prospect__c curProspect = [SELECT Id, Name, First_Name__c, SMS_Disclaimer_Sent__c FROM Prospect__c WHERE Id =: incomingProspectID LIMIT 1];
        List<Prospect__c> curListProspect = [SELECT Id, Name, First_Name__c, SMS_Disclaimer_Sent__c FROM Prospect__c WHERE Id =: incomingProspectID];
        Prospect__c curProspect = curListProspect[0];
        System.debug('*** JB *** CURRENT PROSPECT IS: ' + curProspect);
        //curProspect.Mobile_SMS_Phone__c = formatPhone(msgList[0].getTo());
        if(Integer.valueOf(msgListNumber) > 1){
           // System.debug('*** JB *** HELLO GOOD SIR!!');
            curProspect.SMS_Disclaimer_Sent__c = true;
        }
        update curProspect;
    }
    
    
    
    //****    Twilio sends the phone number as +13125551212.  We have to reformat the string to (312) 555-1212
    public String formatPhone(String fromNumber)
    {
        String areaCode = fromNumber.substring(2,5);
        String prefix = fromNumber.substring(5,8);
        String last4 = fromNumber.substring(8);
        String formattedPhone = '(' + areaCode +')' + ' ' + prefix + '-' + last4;
        System.debug('*** JB *** FORMATTED PHONE IS: ' + formattedPhone);
        return formattedPhone;
    }
    
    public void sendResponseTextMessage(String incomingToNumber, String incomingFromNumber){
        TwilioRestClient client;
        if(!Test.isRunningTest()){
            client = TwilioAPI.getDefaultClient();
        } else {
            String ACCOUNT_SID = 'ACe35fa95cdf61c8bb333d19b6d74c7c30';
            String AUTH_TOKEN = '30026fb2937f1a83808d0fea3f7d7cdd';
            client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
        }
        SMS_Template__c responseMSG = [SELECT Id, Name, SMS_Message__c FROM SMS_Template__c WHERE Name = 'Response' LIMIT 1];
        string response = responseMSG.SMS_Message__c;
        List<TwilioNameValuePair> properties = new List<TwilioNameValuePair>();
        //The FROM number of the incoming SMS Message should be the TO number of the response
        properties.add(new TwilioNameValuePair('To', incomingFromNumber));
        //The TO number of the incoming SMS Message should be the FROM number of the response
        properties.add(new TwilioNameValuePair('From', incomingToNumber));
        //BODY of the Response, from the SOQL Query above
        properties.add(new TwilioNameValuePair('Body', response));
        try{
            TwilioMessage message = client.getAccount().getMessages().create(properties);
        } catch(exception e){
            System.debug('***JB*** SMS Message Error ==========>'+e);
        }   
        
        
      
    }
    
    
    
}