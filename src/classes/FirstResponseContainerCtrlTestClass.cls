@isTest
public class FirstResponseContainerCtrlTestClass {
    
    private static List<Prospect__c> createProspects(Integer numProspects, String postal_code){                                
        //Accepts an integer for # of Prospects to creat and a String for the postal_code 
        //Creates TZ and Zip Code records for the provided postal_code as well as for '90210'
        Time_Zone__c tz = new Time_Zone__c(Name='America/Chicago', Standard_Offset__c = -6, DST__c = true);
        insert tz;
        if(postal_code != '12345'){
            Zip_Code__c z = new Zip_Code__c(Name=postal_code, Time_Zone__c = tz.Id);            
            insert z;
        }
        
        Time_Zone__c test_tz = new Time_Zone__c(Name='America/Los_Angeles', Standard_Offset__c = -8, DST__c = true);
        insert test_tz;
        Zip_Code__c test_z = new Zip_Code__c(Name='90210', Time_Zone__c=test_tz.Id);
        insert test_z;
        
        List<Prospect__c> prospects = new List<Prospect__c>(); 
        for (Integer i=0; i<numProspects; i++){
            Prospect__c p = new Prospect__c(Name='TestProspect'+i, Postal_Code__c = postal_code);
            prospects.add(p); 
        }
        
        insert prospects;
        return prospects;
    }
    
    private static List<Call__c> createCalls(List<Prospect__c> prospects){
        
        //Takes list of Propsects created by method above and creates Call records for each
        
        Call_Pattern__c cp = new Call_Pattern__c();
        cp.Active__c = true;
        cp.Name = 'Test_Call_Pattern';
        cp.Unlimited_Calls__c = true;
        cp.Calls__c = null;
        cp.Consults__c = null;
        
        insert cp;
        
        SMS_Template__c sms = new SMS_Template__c();
        sms.Name = 'Supercool';
        sms.SMS_Message__c = 'Lorem ipsum dolor sit amet';
        
        insert sms;
        
        Email_Template__c eml = new Email_Template__c();
        eml.Name = 'McLovin';
        eml.HTML_Email__c = 'Lorem ipsum dolor sit amet';
        
        insert eml;
        
        Call_Template__c ct = new Call_Template__c(); 
        ct.Call_Pattern__c = cp.Id;
        ct.Attempt__c = 1;
        ct.Min_Hours_Since_Previous_Call__c = 0;
        ct.Team__c = 'A';
        ct.SMS_Template__c = sms.Id;
        ct.Email_Template__c = eml.Id;
        insert ct;                
        
        
        List<Call__c> calls = new List<Call__c>();
        for(Prospect__c p : prospects){
            Call__c c = new Call__c();
            List<RecordType> RT_IDs = [SELECT Id FROM RecordType WHERE Name = 'Prospect Call' AND sObjectType = 'Call__c'];
            Id RT_ID = RT_IDs[0].Id;            
            c.Prospect__c = p.Id;
            c.RecordTypeId = RT_ID;
            c.Call_Pattern__c = cp.Id;
            c.Call_Template__c = ct.Id;
            c.Status__c = 'Not Started';
            c.Attempt__c = 1;
            c.Earliest_Call_Time__c = System.now();
            c.Team__c = 'A';
            calls.add(c);
        }
        insert calls;
        return calls;
    }
    
    
    @isTest static void fetchNextCall(){
        List<Prospect__c> prospects = createProspects(1, '60603');
        List<Call__c> calls = createCalls(prospects);
        
        test.startTest();
        Call__c c = FirstResponseContainerCtrl.getFirstResponseCall('America/Chicago'); 
        test.stopTest();
        
        System.assertNotEquals(null, c.Id);
        
    }
    
    @isTest static void fetchNextCallWhenOverflowTriggered(){
        List<Prospect__c> prospects = createProspects(60, '60603');
        List<Call__c> calls = createCalls(prospects);
        
        User usr = [SELECT Id, Team__c FROM User WHERE Id =: UserInfo.getUserId()];
        String old_usr_team = usr.Team__c;
        usr.Team__c = null;
        update usr;
        
        test.startTest();
        Call__c c = FirstResponseContainerCtrl.getFirstResponseCall('America/Chicago'); 
        test.stopTest();
        
        
        User new_usr = [SELECT Id, Team__c FROM User WHERE Id =: UserInfo.getUserId()];
        new_usr.Team__c = old_usr_team;
        update new_usr;
        
        System.assertNotEquals(null, c.Id);
        
        
    }
    
    @isTest static void fetchProspect(){
        List<Prospect__c> prospects = createProspects(1, '60603');
        Prospect__c p = prospects[0];
        String p_id = p.Id;
        
        test.startTest();
        Prospect__c test_p = FirstResponseContainerCtrl.getFirstResponseProspect(p_id);
        test.stopTest();
        
        System.assertNotEquals(null, test_p.Id);
        
    }
    
    @isTest static void createStandardFollowUp(){
        List<Prospect__c> prospects = createProspects(1, '60603');
        List<Call__c> calls = createCalls(prospects);
        
        Prospect__c p = prospects[0];
        String p_id = p.Id;
        
        Call__c call = calls[0];
        FirstResponseContainerCtrl.createNextCall(p_id, call.Call_Pattern__c, 0, call.Call_Template__c, null, null);    
        
        
    }
    
    @isTest static void createScheduledFollowUp(){
        List<Prospect__c> prospects = createProspects(1, '60603');
        List<Call__c> calls = createCalls(prospects);
        
        Prospect__c p = prospects[0];
        String p_id = p.Id;
        
        Call__c call = calls[0];
        FirstResponseContainerCtrl.createNextCall(p_id, call.Call_Pattern__c, 0, call.Call_Template__c, '11/15/2199 09:00 AM', null);    
        
    }
    
    @isTest static void createMaxFollowUp(){
        List<Prospect__c> prospects = createProspects(1, '60603');
        List<Call__c> calls = createCalls(prospects);
        
        Prospect__c p = prospects[0];
        String p_id = p.Id;
        
        Call__c call = calls[0];
        FirstResponseContainerCtrl.createNextCall(p_id, call.Call_Pattern__c, 25, call.Call_Template__c, null, null);    
        
    }
    
    @isTest static void createMaxScheduledFollowUp(){
        List<Prospect__c> prospects = createProspects(1, '60603');
        List<Call__c> calls = createCalls(prospects);
        
        Prospect__c p = prospects[0];
        String p_id = p.Id;
        
        Call__c call = calls[0];
        
        test.startTest();
        FirstResponseContainerCtrl.createNextCall(p_id, call.Call_Pattern__c, 25, call.Call_Template__c, '11/15/2199 09:00 AM', null);    
        test.stopTest();
    }
    
    @isTest static void getStateCallbackNumber(){
        State__c test_state = new State__c(name='Illinois', Abbreviation__c='IL', Outbound_CallerID__c='312-555-1212');
        insert test_state;
        
        test.startTest();
        String cbnum = FirstResponseContainerCtrl.getStateCBNumber('IL');
        test.stopTest();
        
        System.assertNotEquals(null, cbnum);        
    }
    
}