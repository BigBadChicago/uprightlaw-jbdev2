public without sharing class GoReadyContainerCtrl {
    
    @AuraEnabled
    public static List<String> fetchRawPANumbersList(){
        List<String> PANumbers = new List<String>();
        List<User> usrs = [SELECT Id, Name, Phone, MobilePhone FROM User WHERE Profile.Name = 'Partner Attorney' AND Id =: UserInfo.getUserId() LIMIT 1];
        PANumbers.add(usrs[0].Phone);
        PANumbers.add(usrs[0].MobilePhone);
        return PANumbers;
        

    }
    
    @AuraEnabled
    public static String startApplicationSessionClock() {
        App_Session__c appSess = new App_Session__c();
        appSess.AppSessionStartTime__c = dateTime.now();
        List<User> usrList = [SELECT Id, Name FROM USER WHERE Profile.Name= 'Partner Attorney' AND Id =: UserInfo.getUserId() LIMIT 1];
        appSess.Call_Completed__c = 0.0;
        appSess.Calls_Attempted__c = 0.0;
        appSess.Session_Owner__c = usrList[0].Id;
        appSess.Application__c = 'Go Ready';
        insert appSess;
        return appSess.Id;
    }
    
    
    
    @AuraEnabled
    public static void stopApplicationSessionClock() {
        List<User> users = [SELECT Id, Name FROM User WHERE Profile.Name = 'Partner Attorney' AND Id =: UserInfo.getUserId()];
        List<App_Session__c> appSessions = [SELECT Id, Name, AppSessionStartTime__c, AppSessionEndTime__c, Session_Owner__c FROM App_Session__c WHERE Session_Owner__c =: users[0].Id AND AppSessionEndTime__c = null];
        App_Session__c appSession = appSessions[0];
        appSession.AppSessionEndTime__c = dateTime.now();
        Long calcSessionTime = appSession.AppSessionEndTime__c.getTime() - appsession.AppSessionStartTime__c.getTime();
        appSession.Session_Time__c = Decimal.valueOf((calcSessionTime/1000)/60);
        update appSession;
    }
    
    @AuraEnabled
    public static void increaseCallTime(String callId) {
        List<Call__c> calls = [SELECT Id, Name,Attempt__c, Earliest_Call_Time__c FROM Call__c WHERE Id =: callId];
        Call__c call = calls[0];
        call.Attempt__c = call.Attempt__c +1;
        call.Earliest_Call_Time__c = dateTime.now().addHours(2);
        update call;
    }
    
    @AuraEnabled
    //This method is called on click of Submit of App Settings. 
    //It pulls the User ID, and matches that PA User with open
    //Partner Attorney call types that are on Case Files where 
    //the PA_User matches current running user. It will return 
    //the LIFO Call.
    public static String fetchCall (List<String> callTypes) {
        system.debug('callTypes='+ callTypes);
        
        //Grab running user's ID
        String userId = UserInfo.getUserId();
        
        //Grab current DateTime timestamp (for some reason this only works as a variable in string constructed SOQL queries).
        DateTime currentTime;
        currentTime = system.now();
        
        //Create the start of the non-dynamic portion of the SOQL Query <Keep this query in-sync with fetchCount query below
        String soqlStart;
        soqlStart = 'SELECT Id, Name, Attempt__c, Status__c, Earliest_Call_Time__c, Case_File__r.Partner_Attorney_User__c, Case_File__r.Primary_Contact__c, Case_File__r.DNC_Autodial__c, Case_File__c, RecordType.Name, Reason_for_Call__c FROM Call__c WHERE Status__c != \'Completed\' AND Case_File__r.DNC_Autodial__c != TRUE AND Case_File__r.Partner_Attorney_User__c =: userId AND Earliest_Call_Time__c <=: currentTime AND (';
        
        //Create the end of the non-dynamic portion of the SOQL Query <Keep this query in-sync with fetchCount query below
        String soqlEnd;
        soqlEnd = ') ORDER BY RecordType.Name DESC, Earliest_Call_Time__c DESC';  
        
        //Create the middle of the dynamic portion of the SOQL query.
        //TODO: Form Validation to ensure a callType is always selected
        String soqlMid;
        soqlMid = 'RecordType.Name = \''+callTypes[0]+'\' ';
        
        //For instances where more than 1 Call Type is selected loop through to append WHERE clause strings.
        if(callTypes.size() > 0){
            for (Integer i = 1; i<callTypes.size(); i++){
                soqlMid = soqlMid+' OR RecordType.Name = \''+callTypes[i]+'\' ';
            }
        }
        String soqlQuery = soqlStart+soqlMid+soqlEnd;
        system.debug('soqlQuery = '+soqlQuery);
        
        //Finally query with the constructed string <Keep this query in-sync with fetchCount query below
        List<Call__c> ccs = database.query(soqlQuery);
        
        //Next try to access the first index, if it fails return a no calls statement, otherwise return the object
        try{
            String callJSON = JSON.serialize(ccs[0]);
           }
        catch(System.ListException l)
        {
            String errMsg = l.getMessage();
            system.debug('errMsg: '+errMsg);
            if(errMsg.contains('List index out of bounds')){
                String noCallErr = 'No Calls';
                return noCallErr;
            }
        }
        
        
        //Construct JSON to store Call as object (highest ordered call)
        String callJSON = JSON.serialize(ccs[0]);

        //return Call object in JSON to JS Helper for attrubute setting
        return callJSON;
    }
    
    @AuraEnabled
    //This method is called on click of Submit of App Settings. 
    //It pulls the User ID, and matches that PA User with open
    //Partner Attorney call types that are on Case Files where 
    //the PA_User matches current running user. It will return 
    //total number of call types selected.
    public static Integer fetchCount (List<String> callTypes){
        //Grab running user's ID
        String userId = UserInfo.getUserId();
        
        //Grab current DateTime timestamp (for some reason this only works as a variable in string constructed SOQL queries).
        DateTime currentTime;
        currentTime = system.now();
        
        //Create the start of the non-dynamic portion of the SOQL Query <Keep this query in-sync with fetchCall query above
        String soqlStart;
        soqlStart = 'SELECT Id, Name, Attempt__c, Status__c, Earliest_Call_Time__c, Case_File__r.Partner_Attorney_User__c, Case_File__r.Primary_Contact__c, Case_File__r.DNC_Autodial__c, Case_File__c, RecordType.Name, Reason_for_Call__c FROM Call__c WHERE Status__c != \'Completed\' AND Case_File__r.DNC_Autodial__c != TRUE AND Case_File__r.Partner_Attorney_User__c =: userId AND Earliest_Call_Time__c <=: currentTime AND (';
        
        //Create the end of the non-dynamic portion of the SOQL Query <Keep this query in-sync with fetchCall query above
        String soqlEnd;
        soqlEnd = ') ORDER BY RecordType.Name DESC, Earliest_Call_Time__c DESC';  
        
        //Create the middle of the dynamic portion of the SOQL query.
        String soqlMid;
        soqlMid = 'RecordType.Name = \''+callTypes[0]+'\' ';
        
        //For instances where more than 1 Call Type is selected loop through to append WHERE clause strings.
        if(callTypes.size() > 0){
            for (Integer i = 1; i<callTypes.size(); i++){
                soqlMid = soqlMid+' OR RecordType.Name = \''+callTypes[i]+'\' ';
            }
        }
        String soqlQuery = soqlStart+soqlMid+soqlEnd;
        system.debug('soqlQuery = '+soqlQuery);
        
        //Finally query with the constructed string <Keep this query in-sync with fetchCall query above
        List<Call__c> ccs =database.query(soqlQuery);
        
        //Set query size as the integer to be returned
        Integer callCount = ccs.size();
        
        //Return integer to JS Helper for assignment to cmp attribute
        return callCount;
    }
    
    @AuraEnabled
    //This method is called after submission of App Settings. 
    //It takes in an object ID and returns the SObject.
    public static Case_File__c fetchCaseFile (String caseFileId){
        Case_File__c cf = [SELECT Id, Name, PA_Friendly__c
                           FROM Case_File__c
                           WHERE Case_File__c.Id =: caseFileId
                           LIMIT 1];
        return cf;
    }
    
    @AuraEnabled
    //This method is called after submission of App Settings. 
    //It takes in an object ID and returns the SObject.
    public static Contact fetchContact (String contactId){
        Contact c =[SELECT Id, Name, Phone, MobilePhone, HomePhone, OtherPhone
                    FROM Contact
                    WHERE Contact.Id =: contactId
                    LIMIT 1];
        return c;
    }
    
    @AuraEnabled
    public static void updateAppSessionWithActivity(String appSessionId, String caseFileId, String callId, String contactId) {
        List<User> users = [SELECT Id, Name FROM User WHERE Profile.Name = 'Partner Attorney' AND Id =: UserInfo.getUserId()];
        List<App_Session__c> appSessions = [SELECT Id, Name, AppSessionStartTime__c, AppSessionEndTime__c, Session_Owner__c, Calls_Attempted__c, Last_Action_Occurred__c FROM App_Session__c WHERE Id=:appSessionId];
        App_Session__c appSession = appSessions[0];
        appSession.Calls_Attempted__c = appSession.Calls_Attempted__c+1;
        appSession.Last_Action_Occurred__c = dateTime.now();
        update appSession;
        upr_util.createActivity(users[0].Id, 'GR App - Call Placed', caseFileId, 'Call', 'Completed', '', date.Today(), callId, null, contactId);
    }
    
    @AuraEnabled
    public static void updateAppSessionWithMissedConnection(String appSessionId, String caseFileId, String callId, String contactId) {
        List<User> users = [SELECT Id, Name FROM User WHERE Profile.Name = 'Partner Attorney' AND Id =: UserInfo.getUserId()];
        List<App_Session__c> appSessions = [SELECT Id, Name, AppSessionStartTime__c, AppSessionEndTime__c, Session_Owner__c, Calls_Attempted__c, Last_Action_Occurred__c, Missed_Transfer__c FROM App_Session__c WHERE Id=:appSessionId];
        App_Session__c appSession = appSessions[0];
        appSession.Missed_Transfer__c = true;
        appSession.Last_Action_Occurred__c = dateTime.now();
        update appSession;
        upr_util.createActivity(users[0].Id, 'GR App - Missed Connection', caseFileId, 'Call', 'Completed', '', date.Today(), callId, null, contactId);
    }
    
    
    
}