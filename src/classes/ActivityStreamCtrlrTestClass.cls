@isTest
public class ActivityStreamCtrlrTestClass {
    
    //@isTest private static Prospect__c createProspect(){
    // Prospect__c prp = new Prospect__c();
    //prp.Name = 'Jacob Peterson';
    //prp.First_Name__c = 'Jacob';
    //prp.Last_Name__c = 'Peterson';
    //prp.Personal_Email__c = 'tester@uprightlaw.com';
    //prp.Mobile_SMS_Phone__c = '555-123-4567';
    //prp.Postal_Code__c = '90210';
    
    //insert prp;
    //return prp;
    //}
    private static List<Task> createTasks(Integer numprp, String zip){
        List<Prospect__c> prp = createProspects(numprp, zip);
        List<Task> tsks = new List<Task>();
        for (Integer j=0;j<2;j++){
            //For the newly created prospect add 3 tasks
            tsks.add(new Task(Subject='Calling '+prp[0].First_Name__c+' about stuff -'+j, Status='Not Started', Priority='Normal', WhatId=prp[0].Id ));
        }
        insert tsks;
        return tsks;
    }
    
    private static List<Prospect__c> createProspects(Integer numProspects, String postal_code){                                
        //Accepts an integer for # of Prospects to creat and a String for the postal_code 
        //Creates TZ and Zip Code records for the provided postal_code as well as for '90210'
        Time_Zone__c tz = new Time_Zone__c(Name='America/Chicago', Standard_Offset__c = -6, DST__c = true);
        insert tz;
        if(postal_code != '12345'){
            Zip_Code__c z = new Zip_Code__c(Name=postal_code, Time_Zone__c = tz.Id);            
            insert z;
        }
        
        Time_Zone__c test_tz = new Time_Zone__c(Name='America/Los_Angeles', Standard_Offset__c = -8, DST__c = true);
        insert test_tz;
        Zip_Code__c test_z = new Zip_Code__c(Name='90210', Time_Zone__c=test_tz.Id);
        insert test_z;
        
        List<Prospect__c> prospects = new List<Prospect__c>(); 
        for (Integer i=0; i<numProspects; i++){
            Prospect__c p = new Prospect__c(Name='TestProspect'+i, Postal_Code__c = postal_code);
            prospects.add(p); 
        }
        
        insert prospects;
        return prospects;
    }
    
    private static List<Call__c> createCalls(List<Prospect__c> prospects){
        
        //Takes list of Propsects created by method above and creates Call records for each
        
        Call_Pattern__c cp = new Call_Pattern__c();
        cp.Active__c = true;
        cp.Name = 'Test_Call_Pattern';
        cp.Unlimited_Calls__c = true;
        insert cp;
        
        SMS_Template__c sms = new SMS_Template__c();
        sms.Name = 'Supercool';
        sms.SMS_Message__c = 'Lorem ipsum dolor sit amet';
		
        insert sms;
        
        Email_Template__c eml = new Email_Template__c();
        eml.Name = 'McLovin';
        eml.HTML_Email__c = 'Lorem ipsum dolor sit amet';
		
        insert eml;
        
        Call_Template__c ct = new Call_Template__c(); 
        ct.Call_Pattern__c = cp.Id;
        ct.Attempt__c = 1;
        ct.Min_Hours_Since_Previous_Call__c = 0;
        ct.Team__c = 'A';
        ct.SMS_Template__c = sms.Id;
        ct.Email_Template__c = eml.Id;
        insert ct;
        
        List<Call__c> calls = new List<Call__c>();
        for(Prospect__c p : prospects){
            Call__c c = new Call__c();
            c.Prospect__c = p.Id;
            c.Call_Pattern__c = cp.Id;
            c.Call_Template__c = ct.Id;
            c.Status__c = 'Not Started';
            c.Attempt__c = 1;
            c.Earliest_Call_Time__c = System.now();
            c.Team__c = 'A';
            calls.add(c);
        }
        return calls;
    }
    
    @IsTest static void testNumberofRelatedTasksReturned(){
        
        List<Task> tsks = createTasks(1, '60090');
        test.startTest();
        List<Task> numtsks = ActivityStreamCntrlr.fetchTasks(tsks[0].WhatId);
        test.stopTest();
        
        System.assertNotEquals(null, numtsks);
        System.assertEquals(numtsks[0].WhatId, tsks[0].WhatId);
    }          
}