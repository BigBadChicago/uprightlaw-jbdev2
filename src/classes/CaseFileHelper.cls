public with sharing class CaseFileHelper {
	public static Boolean acceptDummyUpdate = false;

	public static void triggerAction( Boolean isAfter, Boolean isInsert, Boolean isUpdate
					, List<Case_File__c> newCaseFileList, Map<ID, Case_File__c> oldCaseFileMap ) {
		// if not before trigger or not either insert/update, then return
		if( ! isAfter || ( ! isInsert && ! isUpdate ) ) {
			return;
		}
		// return if already running in @future context
		if( system.isFuture() ) {
			return;
		}

		Set<String> courtSet = new Set<String>();

		// collect district court names from new records
		if( isInsert ) {
			for( Case_File__c aFile : newCaseFileList ) {
				courtSet.add( aFile.BK_District__c );
			}
		}

		// collect district court names from records that have changed
		if( isUpdate ) {
			for( Case_File__c aFile : newCaseFileList ) {
				Case_File__c oldFile = oldCaseFileMap.get( aFile.ID );
				// collect records with changed district, case number, stage or filed date
				if( acceptDummyUpdate || aFile.BK_District__c != oldFile.BK_District__c 
						|| aFile.Case_Number__c != oldFile.Case_Number__c
						|| aFile.Stage__c != oldFile.Stage__c
						|| aFile.Filed_Date__c != oldFile.Filed_Date__c ) {
					courtSet.add( aFile.BK_District__c );
				}
			}
		}

		List<String> errorList = new List<String>();
		SchedulableDocketEntriesRefresh.setCourtCodesAndFindFeeds( courtSet, newCaseFileList, errorList );
	}
}