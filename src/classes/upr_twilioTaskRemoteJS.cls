global class upr_twilioTaskRemoteJS {

    public upr_twilioTaskRemoteJS(upr_twilioOutboundCallCustomCtrl controller) {}

	public upr_twilioTaskRemoteJS(ApexPages.StandardController controller) {} // empty constructor

	@RemoteAction
    global static String createCallTask( String caseFileID, String taskDescription, String twilioCallSID ) {
        TwilioRestClient trc = TwilioAPI.getDefaultClient();
        TwilioCall call = trc.getAccount().getCall(twilioCallSID);
        Integer callDurInt = call.getDuration();
    	Case_File__c taskCF = [SELECT Id, Name, Primary_Contact__c, Primary_Contact__r.Id, Primary_Contact__r.FirstName, Primary_Contact__r.LastName, Primary_Contact__r.Phone, Primary_Contact__r.Email FROM Case_File__c WHERE Id =: caseFileID LIMIT 1];
        Task newTask = new Task();
        newTask.WhatId = taskCF.Id;
        newTask.WhoId = taskCF.Primary_Contact__r.Id;
        newTask.Description = taskDescription;
        newTask.CallDurationInSeconds = callDurInt;
        newTask.Status = 'Completed';
        newTask.Subject = 'Call to ' + taskCF.Primary_Contact__r.FirstName + ' ' + taskCF.Primary_Contact__r.LastName + ' Re: ' + taskCF.Name ;
        newTask.Type = 'Call';
        insert newTask;
        return 'success';
    }

}