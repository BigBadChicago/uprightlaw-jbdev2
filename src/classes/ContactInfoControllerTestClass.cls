@isTest
public class ContactInfoControllerTestClass {
    private static List<Prospect__c> createProspects(Integer numProspects, String postal_code){                                
        //Accepts an integer for # of Prospects to creat and a String for the postal_code 
        //Creates TZ and Zip Code records for the provided postal_code as well as for '90210'
        Time_Zone__c tz = new Time_Zone__c(Name='America/Chicago', Standard_Offset__c = -6, DST__c = true);
        insert tz;
        if(postal_code != '12345'){
            Zip_Code__c z = new Zip_Code__c(Name=postal_code, Time_Zone__c = tz.Id);            
            insert z;
        }
        
        Time_Zone__c test_tz = new Time_Zone__c(Name='America/Los_Angeles', Standard_Offset__c = -8, DST__c = true);
        insert test_tz;
        Zip_Code__c test_z = new Zip_Code__c(Name='90210', Time_Zone__c=test_tz.Id);
        insert test_z;
        
        List<Prospect__c> prospects = new List<Prospect__c>(); 
        for (Integer i=0; i<numProspects; i++){
            Prospect__c p = new Prospect__c(Name='TestProspect'+i, Postal_Code__c = postal_code);
            prospects.add(p); 
        }
        
        insert prospects;
        return prospects;
    }
    
    @isTest static void testFetchProspect(){
        List<Prospect__c> prospects = createProspects(1, '60603');
        Prospect__c p = prospects[0];
        
        test.startTest();
        Prospect__c test_p = ContactInfoController.fetchProspect(p.Id);
		System.assertNotEquals(null, test_p.Id);

        test_p.Base_Score__c = 500;       
        ContactInfoController.saveProspect(test_p);
        
        Prospect__c test_p2 = [SELECT Id, Base_Score__c FROM Prospect__c WHERE Id =: test_p.Id];
        System.assertEquals(500, test_p2.Base_Score__c);
        
    }
    
}